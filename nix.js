var sites = {
    "Misc.": {
        "Mastodon": "https://fosstodon.org/",
        "Invidious": "https://www.invidio.us/",
        "Reddit": "https://www.reddit.com/",
        "OSD Forum": "https://discourse.opensourcedesign.net",
        "Discord": "https://discordapp.com/channels/@me",
        "M:TG Life Counter": "http://www.slayweb.com/magic/",
    },
    "Games": { // To find the game ID check the url in the store page or the community page
        "Borderlands 2": "steam://run/49520",
        "Metro 2033": "steam://run/286690",
        "Metro: Last Light": "steam://run/287390",
        "Overlord": "steam://run/11450",
        "Orwell": "steam://run/491950",
        "Valhalla Hills": "steam://run/351910",
    },
    "My stuff": {
        "Seafile": "https://sea.nixnet.xyz",
        "Blog": "https://blog.nixnet.xyz",
        "Gitea": "https://git.nixnet.xyz",
        "GitLab": "https://gitlab.com/Amolith",
        "GitHub": "https://github.com/Amolith?tab=repositories",
        "LUG Email": "http://webmail.lenoirlug.org/",
    },
    "Music": {
        "Redacted": "https://redacted.ch/",
        "ruTorrent": "https://47.135.74.186:3334",
        "ruTracker": "http://rutracker.org/forum/index.php",
    },
    "Design": {
        "CSS Gradients": "https://cssgradient.io/",
        "Undraw": "https://undraw.co/illustrations/",
        "Freepik": "https://freepik.com/",
        "Simple Icons": "https://simpleicons.org/",
        "UI Gradients": "http://uigradients.com/",
        "Logodust": "http://logodust.com"
    },
    "OSINT": {
        "Podcast": "https://inteltechniques.com/podcast.html",
        "IntelTechniques Tools": "https://inteltechniques.com/menu.html",
        "IntelTechniques Forums": "https://inteltechniques.com/forum/index.php",
        "Google Hacking DB": "https://www.exploit-db.com/google-hacking-database",
        "": "",
        "": "",
        "": "",
        "": "",
    },
};

var search = "https://duckduckgo.com/";		// The search engine
var query  = "q";							// The query variable name for the search engine

// ---------- BUILD PAGE ----------
function matchLinks(regex = "") {
    p = document.getElementById("links");
    while (p.firstChild) {
        p.removeChild(p.firstChild);
    }
    match = new RegExp(regex ? regex : ".", "i");
    firstmatch = 0;
    for (i = 0; i < Object.keys(sites).length; i++) {
        matches = false;
        sn = Object.keys(sites)[i];
        section = document.createElement("div");
        section.id = sn;
        section.innerHTML = sn;
        section.className = "section";
        inner = document.createElement("div");
        for (l = 0; l < Object.keys(sites[sn]).length; l++) {
            ln = Object.keys(sites[sn])[l];
            if (match.test(ln)) {
                link = document.createElement("a");
                link.href = sites[sn][ln];
                link.innerHTML = ln;
                if (!firstmatch++ && regex != "") {
                    link.className = "selected";
                    document.getElementById("action").action = sites[sn][ln];
                    document.getElementById("action").children[0].removeAttribute("name");
                }
                inner.appendChild(link);
                matches = true;
            }
        }
        if (!firstmatch || regex == "") {
            document.getElementById("action").action = search;
            document.getElementById("action").children[0].name = query;
        }
        section.appendChild(inner);
        matches ? p.appendChild(section) : false;
    }
}

function displayClock() {
    now = new Date();
    clock = (now.getHours() < 10 ? "0"+now.getHours() : now.getHours())+":"
        +(now.getMinutes() < 10 ? "0"+now.getMinutes() : now.getMinutes())+":"
        +(now.getSeconds() < 10 ? "0"+now.getSeconds() : now.getSeconds());
    document.getElementById("clock").innerHTML = clock;
}

matchLinks();
displayClock();
setInterval(displayClock, 1000);

document.getElementById("action").action = search;
document.getElementById("action").children[0].name = query;
